/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constants */
#define MAX_SIZE_ARRAY 200
#define MAX_NB_TRIALS  210000 // n * n * log(n)


/* Global variables */
static char vertices[MAX_SIZE_ARRAY][MAX_SIZE_ARRAY];
static unsigned char unique_id;


/* Functions */
static inline int read_array_from_file(char filename[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        char line[1024];
        while (fgets(line, sizeof(line), file)) {
            char * ptr = NULL;
            long i = strtol(line, &ptr, 10);
            long j = strtol(ptr, &ptr, 10);
            for ( ; (ptr != NULL) && (j != 0L) ; j = strtol(ptr, &ptr, 10)) {
                vertices[i - 1][j - 1] = 1;
                vertices[j - 1][i - 1] = 1;
            }
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int print_graph(char array[MAX_SIZE_ARRAY][MAX_SIZE_ARRAY]) {
    unsigned char i = 0;
    unsigned char j = 0;
    for (i = 0 ; i < MAX_SIZE_ARRAY ; i++) {
        printf("%d\n", i);
        for (j = 0 ; j < MAX_SIZE_ARRAY ; j++) {
            printf("%d ", array[i][j]);
        }
        printf("\n\n");
    }
    return 0;
}

static inline int print_contractions(unsigned char contractions[]) {
    int i = 0;
    for (i = 0 ; i < MAX_SIZE_ARRAY ; i++){
        if (contractions[i]){
            printf("%d => %d\n", i, contractions[i]);
        }
    }
    printf("\n");
    printf("\n");
}

static inline int pick_remaining_edge(char new_vertices[MAX_SIZE_ARRAY][MAX_SIZE_ARRAY],
                                      unsigned char * i, 
                                      unsigned char * j) {

    unsigned char remaining[MAX_SIZE_ARRAY * MAX_SIZE_ARRAY][2];
    unsigned char line = 0;
    int length = 0;
    for (line = 0 ; line < MAX_SIZE_ARRAY ; line++) {
        unsigned char column = 0;
        for (column = 0 ; column < MAX_SIZE_ARRAY ; column++) {
            if ((line != column) && (new_vertices[line][column])) {
                remaining[length][0] = line;
                remaining[length][1] = column;
                length++;
            }
        }
    }
    int random = rand() % length;
    *i = remaining[random][0];
    *j = remaining[random][1];
    return 0;
}

static inline int add_contraction(unsigned char contractions[],
                                  unsigned char i,
                                  unsigned char j) {
    
    if ((contractions[i]) && (contractions[j])) {
        unsigned char index = 0;
        unsigned char value = contractions[j];
        for (index = 0 ; index < MAX_SIZE_ARRAY ; index++) {
            if (contractions[index] == value) {
                contractions[index] = contractions[i];
            }
        }
    } else if (contractions[i]) {
        contractions[j] = contractions[i];
    } else if (contractions[j]) {
        contractions[i] = contractions[j];
    } else {
        contractions[i] = unique_id;
        contractions[j] = unique_id;
        unique_id++;
    }
}

static inline int get_number_mincut(unsigned char contractions[]) {
    int nb = 0;
    unsigned char i = 0;
    unsigned char id = contractions[i];
    for (i = 0 ; i < MAX_SIZE_ARRAY ; i++) {
        if (contractions[i] == id) {
            unsigned char j = 0;
            for (j = 0; j < MAX_SIZE_ARRAY; j++) {
                if ((i != j) && (contractions[j] != id) && (vertices[i][j])) {
                    nb++;
                }
            }
        }
    }
    return nb;
}

static int find_karger_mincut(void) {

    // Copy the graph
    char new_vertices[MAX_SIZE_ARRAY][MAX_SIZE_ARRAY];
    memcpy(new_vertices, vertices, MAX_SIZE_ARRAY * MAX_SIZE_ARRAY);
    
    // Run the algorithm
    unique_id = 1;
    unsigned char nb_vertices = MAX_SIZE_ARRAY;
    unsigned char contractions[MAX_SIZE_ARRAY];
    memset(contractions, 0, MAX_SIZE_ARRAY);
    while (nb_vertices > 2) {

        // Pick a remaining edge uniformly at random
        unsigned char i = 0;
        unsigned char j = 0;
        pick_remaining_edge(new_vertices, &i, &j);

        // Merge both vertices into a single one
        add_contraction(contractions, i , j);
        unsigned char k = 0;
        for (k = 0 ; k < MAX_SIZE_ARRAY ; k++) {
            if ((i != k) && (new_vertices[j][k])) {
                new_vertices[i][k] = 1;
                new_vertices[k][i] = 1;
                new_vertices[j][k] = 0;
                new_vertices[k][j] = 0;
            }
        }
        new_vertices[i][j] = 0;
        new_vertices[j][i] = 0;

        // Remove self loop
        new_vertices[i][i] = 0;

        // Decrement the number of vertices
        nb_vertices--;
    }

    return get_number_mincut(contractions);
}
    
int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    if (read_array_from_file(argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
    
    int min = MAX_SIZE_ARRAY * MAX_SIZE_ARRAY;
    int trial = 1;
    srand(time(NULL));
    for (trial = 1 ; trial <= MAX_NB_TRIALS ; trial++) {
        int res = find_karger_mincut();
        if ((res) && (res < min)) {
            printf("New min after %d trials: %d\n", trial, res);
            min = res;
        } else if (trial % 1000 == 0) {
            printf("    min after %d trials: %d\n", trial, min);
        }
    }
    printf("Result: %d\n", min);

    return 0;
}

