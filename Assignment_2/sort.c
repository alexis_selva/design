/* Dependancies */
#include <stdio.h>
#include <stdlib.h>


/* Constants */
#define MAX_SIZE_ARRAY 10000

/* Enum */
typedef enum strategy_e {
    PIVOT_FIRST,
    PIVOT_LAST,
    PIVOT_MEDIAN,
} strategy_e;

/* Functions */
static int read_array_from_file(char filename[], int input[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        int i = 0;
        for (i = 0 ; i < MAX_SIZE_ARRAY ; i++) {
           fscanf(file, "%d", &input[i]);
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline void print_array(int input[]) {
    int i = 0;
    for (i = 0 ; i < MAX_SIZE_ARRAY ; i++) {
        printf("%d \n", input[i]);
    }
    printf("\n");
}        

static inline void swap(int input[], int i, int j) {
    //input[i] ^= input[j];
    //input[j] ^= input[i];
    //input[i] ^= input[j];
    int tmp = input[i];
    input[i] = input[j];
    input[j] = tmp;
}

static inline int determine_median_of_three(int input[], int first, int middle, int last) {
    if ((input[first] - input[middle]) * (input[last] - input[first]) >= 0) {
        return first;
    } else if ((input[middle] - input[first]) * (input[last] - input[middle]) >= 0) {
        return middle;
    } else {
        return last;
    }
}

static inline int select_pivot(int input[], int first, int middle, int last, strategy_e strategy) {
    switch (strategy) {
        case PIVOT_FIRST:
            break;
        case PIVOT_LAST:
            swap(input, first, last);
            break;
        case PIVOT_MEDIAN:
            {
                int median = determine_median_of_three(input, first, middle, last);
                swap(input, first, median);
            }
            break;
    }
    return first;
}

static inline size_t partition_and_count(int input[], int * pivot, int left, int right) {
    int i = left + 1;
    int j = i;
    for (j = i ; j <= right ; j++) {
        if ((j != *pivot) && (input[j] < input[*pivot])) {
            swap(input, i, j);
            i++;
        }
    }
    swap(input, *pivot, i - 1);
    *pivot = i - 1;
    return right - left;
}

static inline size_t quicksort(int input[], int left, int right, strategy_e strategy) {
    if (right <= left) {
        return 0;
    }
    int pivot = select_pivot(input, left, (right + left) / 2, right, strategy);
    size_t comparisons_1 = partition_and_count(input, &pivot, left, right);
    size_t comparisons_2 = quicksort(input, left, pivot - 1, strategy);
    size_t comparisons_3 = quicksort(input, pivot + 1, right, strategy);
    return comparisons_1 + comparisons_2 + comparisons_3;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    int input[MAX_SIZE_ARRAY];
    if (read_array_from_file(argv[1], input) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
    int tmp[MAX_SIZE_ARRAY];
    size_t result = quicksort(input, 0, MAX_SIZE_ARRAY- 1, PIVOT_LAST);
    printf("%lu\n", result);

    //print_array(input);

    return 0;
}
