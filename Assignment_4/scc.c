/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constants */
#define MAX_SIZE_ARRAY 875714
#define NB_BIGGEST_SCC 5
typedef enum lecture_t {
    NORMAL = 0,
    INVERSED = 1,
    SORTED = 2,
} lecture_t;


/* Structures */
typedef struct edge_t {
    unsigned int vertex;
    struct edge_t * next;
} edge_t;

typedef struct queue_t {
    unsigned char explored;
    int nb;
    int nb_scc;
    edge_t * first;
    edge_t * last;
} queue_t;


/* Global variables */
static queue_t vertices[MAX_SIZE_ARRAY];
static queue_t inv_vertices[MAX_SIZE_ARRAY];
static int leaders[MAX_SIZE_ARRAY];
static int leaders_nb[MAX_SIZE_ARRAY];
static int times[MAX_SIZE_ARRAY];
static int finish_time;
static int source;

/* Functions */
static inline int size(queue_t * q){
    return q->nb;
}

static inline int is_empty(queue_t * q) {
    return (q->first == NULL);
}

static inline int enqueue(queue_t * q, int v) {
    edge_t * old = q->last;
    q->last = malloc(sizeof(edge_t));
    if (q->last) {
        q->last->vertex = v;
        q->last->next = NULL;
        if (is_empty(q)) { 
            q->first = q->last;
        } else {
            old->next = q->last;
        }
        q->nb++;
        return 0;
    }
    return -1;
}

static inline int dequeue(queue_t * q) {
    if (is_empty(q)) {
        return -1;
    }
    int vertex = q->first->vertex;
    q->first = q->first->next;
    q->nb--;
    if (is_empty(q)) {
        q->last = NULL;
    }
    return vertex;
}

static inline int read_array_from_file(char filename[], lecture_t type) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        char line[1024];
        while (fgets(line, sizeof(line), file)) {
            char * ptr = NULL;
            long i = strtol(line, &ptr, 10);
            long j = strtol(ptr, &ptr, 10);
            for ( ; (ptr != NULL) && (j != 0L) ; j = strtol(ptr, &ptr, 10)) {
                switch (type) {
                    case NORMAL:
                        {
                            if (enqueue(&vertices[i - 1], j - 1) != 0) {
                                printf("Error while enqueuing: %d -> %d\n", i - 1, j - 1);
                                exit(1);
                            }
                        }
                        break;

                    case INVERSED:
                        {
                            if (enqueue(&inv_vertices[j - 1], i - 1) != 0) {
                                printf("Error while enqueuing: %d -> %d\n", j - 1, i - 1);
                                exit(1);
                            }
                        }
                        break;

                    case SORTED:
                        {
                            int new_line = times[i - 1];
                            int new_column = times[j - 1];
                            if (enqueue(&vertices[new_line], new_column) != 0) {
                                printf("Error while enqueuing: %d -> %d\n", new_line, new_column);
                                exit(1);
                            }
                        }
                        break;
                }
            }
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int print_graph(queue_t array[MAX_SIZE_ARRAY]) {
    unsigned char i = 0;
    for (i = 0 ; i < MAX_SIZE_ARRAY ; i++) {
        printf("%d: ", i);
        queue_t * q = &vertices[i];
        edge_t * e = q->first;
        while (e) {
            printf("%d ", e->vertex);
            e = e->next;
        }
        printf("\n\n");
    }
    return 0;
}

static inline int depth_first_search(queue_t array[MAX_SIZE_ARRAY], int index) {
    leaders[index]= source;
    leaders_nb[source] += 1;
    queue_t * q = &array[index];
    q->explored = 1;
    edge_t * e = q->first;
    while (e) {
        if (!array[e->vertex].explored) {
            depth_first_search(array, e->vertex);
        }
        e = e->next;
    }
    times[index] = finish_time;
    finish_time++;
}

static int depth_first_search_loop(queue_t array[MAX_SIZE_ARRAY]) {
    finish_time = 0;
    source = 0;
    int index = 0;
    memset(times, 0, MAX_SIZE_ARRAY * sizeof (int));
    memset(leaders, 0, MAX_SIZE_ARRAY * sizeof(int));
    memset(leaders_nb, 0, MAX_SIZE_ARRAY * sizeof(int));
    for (index = MAX_SIZE_ARRAY - 1 ; index >= 0; index--) {
        queue_t * q = &array[index]; 
        if (!q->explored) {
            source = index;
            depth_first_search(array, index);
        }
    }
}

static void swap(int array[MAX_SIZE_ARRAY], int i, int j) {
    int swap = array[i];
    array[i] = array[j];
    array[j] = swap;
}

static void sink(int array[MAX_SIZE_ARRAY], int k, int nb) {
    while (2 * k <= nb) {
        int j = 2 * k;
        if ((j < nb) && (array[j - 1] > array[j])) {
            j++;
        }
        if (array[k - 1] <= array[j - 1]) {
            break;
        }
        swap(array, k - 1, j - 1);
        k = j;
    }
}

static void sort(int array[MAX_SIZE_ARRAY]) {
    int nb = MAX_SIZE_ARRAY;
    int k = 0;
    for (k = nb/2; k >= 1; k--) {
        sink(array, k, nb);
    }
    while (nb > 1) {
        swap(array, 0, nb - 1);
        nb--;
        sink(array, 1, nb);
    }
}

static inline int print_biggest_scc(int array[MAX_SIZE_ARRAY]) {
    int index = 0;
    sort(array);
    for (index = 0 ; index < NB_BIGGEST_SCC - 1 ; index++) {
        printf("%d,", array[index]);
    }
    printf("%d\n", array[NB_BIGGEST_SCC - 1]);
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    if (read_array_from_file(argv[1], INVERSED) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
   
    // DFS on reversed graph
    depth_first_search_loop(inv_vertices);
    
    // Reorder by considering the finished times
    if (read_array_from_file(argv[1], SORTED) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }

    // DFS on graph
    depth_first_search_loop(vertices);

    // Print the 5th biggest sizes of the computed strongly connected components
    print_biggest_scc(leaders_nb);

    return 0;
}

