These are the programming assignments relative to design (1st part):

* Assignment 1: computing the number of inversions in the file given
* Assignment 2: computing the total number of comparisons used to sort the given input file by QuickSort
* Assignment 3: coding up and run the randomized contraction algorithm for the min cut problem 
* Assignment 4: coding up  and run the algorithm for computing strongly connected components (SCCs)
* Assignment 5: coding up Dijkstra's shortest-path algorithm
* Assignment 6: implementing a variant of the 2-SUM algorithm and the "Median Maintenance" algorithm

For more information, I invite you to have a look at https://www.coursera.org/course/algo