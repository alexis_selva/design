/* Dependancies */
#include <stdio.h>
#include <stdlib.h>


/* Constants */
#define MAX_SIZE_ARRAY 100000


/* Functions */
static int read_array_from_file(char filename[], int input[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        size_t i = 0;
        for (i = 0 ; i < MAX_SIZE_ARRAY ; i++) {
           fscanf(file, "%d", &input[i]);
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static size_t merge_and_count(int input[], int tmp[], size_t low, size_t middle, size_t high){
    size_t k = low;

    for (k = low ; k <= high ; k++){
        tmp[k] = input[k];
    }

    size_t i = low;
    size_t j = middle + 1;
    size_t inversions = 0;
    for (k = low ; k <= high ; k++) {
        if (i > middle) {
            continue;
        } else if (j > high) {
            input[k] = tmp[i++];
        } else if (tmp[j] < tmp[i]) {
            input[k] = tmp[j++];
            inversions += middle - i + 1;
        } else {
            input[k] = tmp[i++];
        }
    }
    
    return inversions;
}

static size_t sort_and_count (int input[], int tmp[], size_t low, size_t high) {
    if (high <= low) {
        return 0;
    }
    size_t middle = low + (high - low) / 2;
    size_t inversions_1 = sort_and_count(input, tmp, low, middle);
    size_t inversions_2 = sort_and_count(input, tmp, middle + 1, high);
    size_t inversions_3 = merge_and_count(input, tmp, low, middle, high);
    
    return inversions_1 + inversions_2 + inversions_3;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    int input[MAX_SIZE_ARRAY];
    if (read_array_from_file(argv[1], input) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
    int tmp[MAX_SIZE_ARRAY];
    size_t result = sort_and_count(input, tmp, 0, MAX_SIZE_ARRAY - 1);
    printf("%lu\n", result);

    return 0;
}
