/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constant */
#define MAX_NB_MEDIANS  10000


/* Enumerators */
typedef enum color_t {
    BLACK = 0,
    RED = 1,
} color_t;


/* Structures */
typedef struct node_t {
    int key;
    struct node_t * left;
    struct node_t * right;
    unsigned char color;
    unsigned int nb;
} node_t;


/* Globabl variables */
node_t * root = NULL;


/* Functions */
static inline node_t * allocate_node(int key, color_t color, unsigned int nb) {
    node_t * new_node = malloc(sizeof(node_t));
    if (new_node) {
        new_node->key = key;
        new_node->color = color;
        new_node->nb = nb;
    }
    return new_node;
}

static inline int is_red(node_t * node) {
    if (!node) return 0;
    return (node->color == RED);
}

static inline unsigned int size_node(node_t * node) {
    if (!node) return 0;
    return node->nb;
}

static inline unsigned int size(void) {
    return size_node(root);
}

static inline int is_empty(void) {
    return (root == NULL);
}

static inline node_t * get_node(node_t * node, int key) {
    while (node) {
        if (key < node->key) {
            node = node->left;
        } else if (key > node->key) {
            node = node->right;
        } else {
            return node;
        }
    }
    return NULL;
}

static inline node_t * get_root(int key) {
    return get_node(root, key);
}

static inline int contains(int key) {
    return (get_root(key) != NULL);
}

static inline node_t * rotate_right(node_t * h) {
    node_t * x = h->left;
    h->left = x->right;
    x->right = h;
    x->color = x->right->color;
    x->right->color = RED;
    x->nb = h->nb;
    h->nb = size_node(h->left) + size_node(h->right) + 1;
    return x;
}

static inline node_t * rotate_left(node_t * h) {
    node_t * x = h->right;
    h->right = x->left;
    x->left = h;
    x->color = x->left->color;
    x->left->color = RED;
    x->nb = h->nb;
    h->nb = size_node(h->left) + size_node(h->right) + 1;
    return x;
}

static inline int flip_colors(node_t * h) {
    h->color ^= h->color;
    h->left->color ^= h->left->color;
    h->right->color ^= h->right->color;
}

static inline node_t * put_node(node_t * node, int key) {
    if (node == NULL) {
        return allocate_node(key, RED, 1);
    }
    if (key < node->key) {
        node->left = put_node(node->left, key);
    } else if (key > node->key) {
        node->right = put_node(node->right, key);
    } 

    if ((is_red(node->right)) && (!is_red(node->left))) {
        node = rotate_left(node);
    }
    if ((is_red(node->left)) && (is_red(node->left->left))) {
        node = rotate_right(node);
    }
    if ((is_red(node->left)) && (is_red(node->right))) {
        flip_colors(node);
    }
    node->nb = size_node(node->left) + size_node(node->right) + 1;

    return node;
}

static inline int put_root(int key) {
    root = put_node(root, key);
    root->color = BLACK;
    return 0;
}

static inline node_t * select_node(node_t * x, unsigned int k) {
    int t = size_node(x->left);
    if (t > k) { 
        return select_node(x->left, k);
    } else if (t < k) {
        return select_node(x->right, k - t - 1);
    } else {
        return x;
    }
}

static inline node_t * select_root(unsigned int k) {
    if (k >= size()) return NULL;
    return select_node(root, k);
}

static inline int read_from_file(char filename[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        unsigned int i = 0;
        int medians = 0;
        for ( ; i < MAX_NB_MEDIANS ; i++) {
            int value;
            fscanf(file, "%d", &value);
            if (put_root(value) < 0) {
                printf("Error while puting: %d\n", value);
                exit(1);
            }
            node_t * median = select_root(i/2);
            if (!median) {
                printf("Error while selecting: %d\n", i/2);
                exit(1);
            }
            medians += median->key;
        }
        fclose(file);
        return medians % MAX_NB_MEDIANS;
    }
    
    return -1;
}

int main (int argc, char * argv[]) {

    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }

    // Read the value
    int medians = read_from_file(argv[1]);
    printf("Result = %d\n", medians);

    return 0;
}

