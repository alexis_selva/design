/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constant */
#define MAX_NB_KEYS     1000000
#define MAX_NB_BUCKETS  100003 // prime number


/* Structures */
typedef struct entry_t {
    long long int key;
    struct entry_t * next;
} entry_t;

typedef struct hashtable_t {
    entry_t * buckets[MAX_NB_BUCKETS];
    unsigned int nb;
} hashtable_t;


/* Functions */
static inline unsigned int size(hashtable_t * table){
    return table->nb;
}

static inline int is_empty(hashtable_t * table) {
    return size(table) == 0;
}

static inline unsigned int hash(long long int key) {
    unsigned int value = key + (key << 3) + (key << 12);
    return (value & 0x7fffffff) % MAX_NB_BUCKETS;
}

static inline int find(hashtable_t * table, long long int key) {
    
    // Determine the corresponding bucket
    unsigned int bucket = hash(key);

    // Lookup the key into the list
    entry_t * entry = table->buckets[bucket];
    while (entry) {
        if (entry -> key == key) {
            return 1;
        }
        entry = entry->next;
    }

    return 0;
}

static inline int add(hashtable_t * table, long long int key) {

    // Is the key present into the table ?
    if (find(table, key) == 0) {

        // Allocate and insert a new entry at the head
        entry_t * new_entry = malloc(sizeof(entry_t));
        if (new_entry) {
            new_entry->key = key;
            unsigned int bucket = hash(key);
            new_entry->next = table->buckets[bucket];
            table->buckets[bucket] = new_entry;
            table->nb++;
            return 1;
        }
        return -1;
    }
    return 0;
}

static inline analyse(hashtable_t *table, long long int t) {
    int bucket = 0;
    for ( ; bucket < MAX_NB_BUCKETS ; bucket++) {
        entry_t * entry = table->buckets[bucket];
        while (entry) {
            long long int x = entry->key;
            long long int y = t - x;
            if ((x != y) && find(table, y)) {
                return 1;
            }
            entry = entry->next;
        }
    }
    return 0;
}

static inline int read_from_file(hashtable_t * table, char filename[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        size_t i = 0;
        for (i = 0 ; i < MAX_NB_KEYS ; i++) {
            long long int value;
            fscanf(file, "%lld", &value);
            if (add(table, value) < 0) {
                printf("Error while adding: %lld\n", value);
                exit(1);
            }
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }

    // Initialize table
    hashtable_t table;
    memset(&table, 0, sizeof(hashtable_t));

    // Read the value
    if (read_from_file(&table, argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }

    // Analyse the values
    int result = 0;
    int sum = -10000;
    for ( ; sum <= 10000 ; sum++) {
        if (sum % 100 == 0) {
            printf("Sum %d\n", sum);
        }
        result += analyse(&table, sum);
    }
    printf("Result = %d\n", result);

    return 0;
}

